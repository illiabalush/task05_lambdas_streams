package task_1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

interface Calculable {
    int operation(int firstArgument, int secondArgument, int thirdArgument);
}

public class Application {

    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        Calculable maxValue = new Application()::getMaxValue;
        logger.info(maxValue.operation(9, 18, 7));
        Calculable averageValue = (firstArgument, secondArgument, thirdArgument) ->
                (firstArgument + secondArgument + thirdArgument) / 3;
        logger.info(averageValue.operation(10, 11, 12));
    }

    private int getMaxValue(int firstArgument, int secondArgument, int thirdArgument) {
        return (secondArgument > firstArgument && secondArgument > thirdArgument) ? secondArgument
                : (thirdArgument > firstArgument && thirdArgument > secondArgument) ? thirdArgument : firstArgument;
    }
}
