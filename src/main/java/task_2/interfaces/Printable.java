package task_2.interfaces;

public interface Printable {
    void print();
}
