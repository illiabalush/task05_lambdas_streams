package task_2.interfaces;

public interface Command {
    void execute();
}
