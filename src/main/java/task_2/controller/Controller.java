package task_2.controller;

import task_2.command.ClearString;
import task_2.interfaces.Command;
import task_2.command.DoubleString;
import task_2.command.LowerCase;
import task_2.view.View;

public class Controller {
    private String string;
    private Command command;

    public Controller(String string) {
        this.string = string;
    }

    public void doubleString() {
        View.history.push(
        new DoubleString(this) {
            @Override
            public void execute() {
                getController().setString(string + string);
            }
        });
        View.history.getLast().execute();
    }

    public void toLowerCase() {
        command = new LowerCase(this)::execute;
        command.execute();
        View.history.push(command);
    }

    public void toUpperCase() {
        command = () -> setString(string.toUpperCase());
        command.execute();
        View.history.push(command);
    }

    public void clearString() {
        command = new ClearString(this);
        command.execute();
        View.history.push(command);
    }

    public void setString(String string) {
        this.string = string;
    }

    public String getString() {
        return string;
    }

    public void setCommand(Command command) {
        this.command = command;
        command.execute();
    }
}
