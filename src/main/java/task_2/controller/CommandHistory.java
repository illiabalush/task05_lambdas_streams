package task_2.controller;

import task_2.interfaces.Command;

import java.util.Stack;

public class CommandHistory {
    private Stack<Command> history = new Stack<>();

    public void push(Command command) {
        history.push(command);
    }

    public Command pop() {
        return history.pop();
    }

    public Command getLast() {
        return history.peek();
    }

    public boolean isEmpty() {
        return history.empty();
    }
}
