package task_2.view;

import task_2.controller.CommandHistory;
import task_2.controller.Controller;
import task_2.interfaces.Command;
import task_2.interfaces.Printable;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ConsoleView extends View {
    private Map<String, String> menu;
    private Map<String, Printable> menuFunctions;
    private Scanner scanner;

    public ConsoleView() {
        controller = new Controller("");
        history = new CommandHistory();
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Double String");
        menu.put("2", "2 - to lower case");
        menu.put("3", "3 - to upper case");
        menu.put("4", "4 - clear string");
        menu.put("5", "5 - change value");
        menu.put("6", "6 - showStreamExpression value");
        menu.put("7", "7 - undo action");
        menu.put("Q", "Q - exit");
        menuFunctions = new LinkedHashMap<>();
        menuFunctions.put("1", controller::doubleString);
        menuFunctions.put("2", controller::toLowerCase);
        menuFunctions.put("3", controller::toUpperCase);
        menuFunctions.put("4", controller::clearString);
        menuFunctions.put("5", this::fillString);
        menuFunctions.put("6", this::showString);
        menuFunctions.put("7", this::undo);
    }

    private void showString() {
        logger.info(controller.getString());
    }

    private void fillString() {
        logger.info("Enter string: ");
        scanner = new Scanner(System.in);
        controller.setString(scanner.nextLine());
    }

    private void undo() {
        if(!history.isEmpty()) {
            Command command = history.pop();
            controller.setCommand(command);
        }
    }

    @Override
    protected void showMenu() {
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    @Override
    public void start() {
        String keyMenu;
        fillString();
        do {
            showMenu();
            scanner = new Scanner(System.in);                 //?????
            logger.info("Please, select menu point:");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                menuFunctions.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
