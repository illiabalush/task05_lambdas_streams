package task_2.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task_2.controller.CommandHistory;
import task_2.controller.Controller;

public abstract class View {
    protected Controller controller;
    public static CommandHistory history;
    protected static Logger logger = LogManager.getLogger(View.class);

    protected abstract void showMenu();
    public abstract void start();
}
