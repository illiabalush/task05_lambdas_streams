package task_2.command;

import task_2.controller.Controller;
import task_2.interfaces.Command;

abstract public class DoubleString implements Command {
    private Controller controller;
    public DoubleString(Controller controller) {
        this.controller = controller;
    }

    public Controller getController() {
        return controller;
    }
}
