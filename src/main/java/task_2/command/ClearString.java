package task_2.command;

import task_2.controller.Controller;
import task_2.interfaces.Command;

public class ClearString implements Command {

    private Controller controller;

    public ClearString(Controller controller) {
        this.controller = controller;
    }

    private void clearString() {
        controller.setString("");
    }

    @Override
    public void execute() {
        clearString();
    }
}
