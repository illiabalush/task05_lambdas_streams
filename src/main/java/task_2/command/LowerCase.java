package task_2.command;

import task_2.controller.Controller;
import task_2.interfaces.Command;

public class LowerCase implements Command {
    private Controller controller;

    public LowerCase(Controller controller) {
        this.controller = controller;
    }

    private void toLowerCase() {
        controller.setString(controller.getString().toLowerCase());
    }

    @Override
    public void execute() {
        toLowerCase();
    }
}
