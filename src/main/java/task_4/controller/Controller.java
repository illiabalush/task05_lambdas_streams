package task_4.controller;

import task_4.model.Model;

import java.util.List;

public class Controller {
    private Model model;

    public Controller(List<String> list) {
        model = new Model(list);
    }

    public Model getModel() {
        return model;
    }
}
