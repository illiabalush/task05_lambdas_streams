package task_4;

import task_4.view.View;

public class Main {
    public static void main(String[] args) {
        new View().start();
    }
}
