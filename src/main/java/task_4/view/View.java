package task_4.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task_4.controller.Controller;
import task_4.interfaces.Printable;

import java.util.*;

public class View {
    private static Logger logger = LogManager.getLogger(View.class);
    private Scanner scanner;
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, String> languageMenu;
    private Map<String, Printable> menuFunctions;
    private Map<String, Printable> languageMenuFunctions;

    private Locale[] locales = {
            new Locale("en", "en"),
            new Locale("uk", "ua"),
            new Locale("fr", "fr"),
            new Locale("de", "de")
    };

    public View() {
        scanner = new Scanner(System.in);
        loadMenu(locales[0]);
        logger.info("Enter list: ");
        controller = new Controller(getList());

        menuFunctions = new LinkedHashMap<>();
        menuFunctions.put("1", this::enterList);
        menuFunctions.put("2", this::showList);
        menuFunctions.put("3", this::getNumberUniqueWords);
        menuFunctions.put("4", this::sortUniqueWords);
        menuFunctions.put("5", this::getWordsCount);
        menuFunctions.put("6", this::getOccurrenceEachSymbol);
        menuFunctions.put("7", this::changeLanguage);

        languageMenu = new LinkedHashMap<>();
        languageMenu.put("1", "1 - English");
        languageMenu.put("2", "2 - Українська");
        languageMenu.put("3", "3 - Francais");
        languageMenu.put("4", "4 - Deutsch");
        languageMenuFunctions = new LinkedHashMap<>();
        languageMenuFunctions.put("1", this::changeToEnglish);
        languageMenuFunctions.put("2", this::changeToUkrainian);
        languageMenuFunctions.put("3", this::changeToFrancais);
        languageMenuFunctions.put("4", this::changeToDeutsch);
    }

    private List<String> getList() {
        List<String> list = new ArrayList<>();
        String value;
        do {
            value = scanner.nextLine();
            list.add(value);
        } while (!value.equals(""));
        list.remove(list.size() - 1);
        return list;
    }

    private void loadMenu(Locale currentLocale) {
        menu = new LinkedHashMap<>();
        ResourceBundle language = ResourceBundle.getBundle("menu", currentLocale);
        for (String s : language.keySet()) {
            menu.put(s, language.getString(s));
        }
    }

    private void showLanguageMenu() {
        for (String s : languageMenu.values()) {
            logger.info(s);
        }
    }

    private void enterList() {
        logger.info("Set list");
        controller.getModel().setList(getList());
    }

    private void showList() {
        logger.info(controller.getModel().getList());
    }

    private void getNumberUniqueWords() {
        logger.info(controller.getModel().getNumberUniqueWords());
    }

    private void sortUniqueWords() {
        logger.info(controller.getModel().sortUniqueWords());
    }

    private void getWordsCount() {
        logger.info(controller.getModel().getWordsCount());
    }

    private void getOccurrenceEachSymbol() {
        controller.getModel().getOccurrenceEachSymbol(this);
    }

    public void showStreamExpression(String string) {
        logger.info(string);
    }

    private void changeLanguage() {
        String keyMenu;
        showLanguageMenu();
        scanner = new Scanner(System.in);
        logger.info("Please, select menu point:");
        keyMenu = scanner.nextLine().toUpperCase();
        try {
            languageMenuFunctions.get(keyMenu).print();
        } catch (Exception ignored) {
        }
    }

    private void changeToEnglish() {
        loadMenu(locales[0]);
    }

    private void changeToUkrainian() {
        loadMenu(locales[1]);
    }

    private void changeToFrancais() {
        loadMenu(locales[2]);
    }

    private void changeToDeutsch() {
        loadMenu(locales[3]);
    }

    private void showMenu() {
        for (String s : menu.values()) {
            logger.info(s);
        }
    }

    public void start() {
        String keyMenu;
        do {
            showMenu();
            scanner = new Scanner(System.in);
            logger.info("Please, select menu point:");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                menuFunctions.get(keyMenu).print();
            } catch (Exception ignored) {

            }
        } while (!keyMenu.equals("Q"));
    }
}
