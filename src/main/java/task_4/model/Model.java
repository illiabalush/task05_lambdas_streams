package task_4.model;

import java.util.List;
import java.util.Map;

import task_4.view.View;

import java.util.stream.Collectors;

public class Model {
    private List<String> list;

    public Model(List<String> list) {
        this.list = list;
    }

    public int getNumberUniqueWords() {
        return (int) list.stream().distinct().count();
    }

    public List<String> sortUniqueWords() {
        return list.stream().distinct().sorted().collect(Collectors.toList());
    }

    public Map<String, Long> getWordsCount() {
        return list.stream().collect(Collectors.groupingBy(String::toString, Collectors.counting()));
    }

    public void getOccurrenceEachSymbol(View view) {
        list.stream().forEach(s ->
                view.showStreamExpression(s + " " + s.chars().mapToObj(i -> (char) i)
                        .filter(Character::isLowerCase)
                        .collect(Collectors.groupingBy(Character::charValue, Collectors.counting()))));
    }

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }
}
