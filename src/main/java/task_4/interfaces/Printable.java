package task_4.interfaces;

public interface Printable {
    void print();
}
