package task_3.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task_3.controller.Controller;
import task_3.interfaces.Printable;

import java.util.*;

public class View {
    private static Logger logger = LogManager.getLogger(View.class);
    private final int SIZE = 10;
    private Scanner scanner;
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> menuFunctions;

    public View() {
        controller = new Controller(getRandomList());
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - showStreamExpression list");
        menu.put("2", "2 - get average value");
        menu.put("3", "3 - get min value");
        menu.put("4", "4 - get max value");
        menu.put("5", "5 - get sum list using stream");
        menu.put("6", "6 - get sum list using stream reduce");
        menu.put("7", "7 - get number of values that are bigger than average");
        menu.put("Q", "Q - exit");
        menuFunctions = new LinkedHashMap<>();
        menuFunctions.put("1", this::showList);
        menuFunctions.put("2", this::showAverageValue);
        menuFunctions.put("3", this::showMinValue);
        menuFunctions.put("4", this::showMaxValue);
        menuFunctions.put("5", this::showSumListByStream);
        menuFunctions.put("6", this::showSumListByReduce);
        menuFunctions.put("7", this::showNumberValuesBiggerThanAverage);
    }

    private void showList() {
        logger.info(controller.getModel().getList());
    }

    private void showAverageValue() {
        logger.info(controller.getModel().getAverageValue());
    }

    private void showMinValue() {
        logger.info(controller.getModel().getMinValue());
    }

    private void showMaxValue() {
        logger.info(controller.getModel().getMaxValue());
    }

    private void showSumListByStream() {
        logger.info(controller.getModel().getSumByStream());
    }

    private void showSumListByReduce() {
        logger.info(controller.getModel().getSumByReduce());
    }

    private void showNumberValuesBiggerThanAverage() {
        logger.info(controller.getModel().getCountBiggerThanAverage());
    }

    private List<Integer> getRandomList() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < SIZE; i++) {
            list.add((int) (Math.random() * 10));
        }
        return list;
    }

    private void showMenu() {
        for (String s : menu.values()) {
            logger.info(s);
        }
    }

    public void start() {
        String keyMenu;
        do {
            showMenu();
            scanner = new Scanner(System.in);
            logger.info("Please, select menu point:");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                menuFunctions.get(keyMenu).print();
            } catch (Exception ignored) {

            }
        } while (!keyMenu.equals("Q"));
    }
}

