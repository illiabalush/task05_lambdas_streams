package task_3.controller;

import task_3.model.Model;

import java.util.List;

public class Controller {
    private Model model;

    public Controller(List<Integer> list) {
        model = new Model(list);
    }

    public Model getModel() {
        return model;
    }
}
