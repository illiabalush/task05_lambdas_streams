package task_3.interfaces;

public interface Printable {
    void print();
}
