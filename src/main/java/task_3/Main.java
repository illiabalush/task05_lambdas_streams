package task_3;

import task_3.view.View;

public class Main {
    public static void main(String[] args) {
        new View().start();
    }
}
