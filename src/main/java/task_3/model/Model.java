package task_3.model;

import java.util.List;

public class Model {
    private List<Integer> list;

    public Model(List<Integer> list) {
        this.list = list;
    }

    public int getSumByStream() {
        return list.stream().mapToInt(Integer::intValue).sum();
    }

    public int getSumByReduce() {
        return list.stream().mapToInt(Integer::intValue).reduce((a, b) -> a + b).getAsInt();
    }

    public int getMinValue() {
        return list.stream().mapToInt(Integer::intValue).min().getAsInt();
    }

    public int getMaxValue() {
        return list.stream().mapToInt(Integer::intValue).max().getAsInt();
    }

    public double getAverageValue() {
        return list.stream().mapToInt(Integer::intValue).average().getAsDouble();
    }

    public int getCountBiggerThanAverage() {
        return (int) list.stream().filter(i -> i > getAverageValue()).count();
    }

    public List<Integer> getList() {
        return list;
    }
}
